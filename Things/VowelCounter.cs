﻿using AllTheTinyThings.FSM.Menus;
using System.Linq;

using System.Collections.Generic;

namespace AllTheTinyThings
{
	internal class VowelCounter
	{
		// count how many Vowvels are in a thing, then tell the user how many there were, and maybe add how many of each?
		Dictionary<char, int> AmountOfSetVowel = new Dictionary<char, int>();

		public int GetVowelCount(string str)
		{
			return str.Count(CommonActions.vowels.Contains);
		}
		public Dictionary<char,int> GetExactVowelCount(string str)
		{
			str = str.ToLower();
			foreach(char c in str)
			{
				if (CommonActions.vowels.Contains(c))
				{
					if (!AmountOfSetVowel.ContainsKey(c))
						AmountOfSetVowel.Add(c, 1);
					else
					{
						int t = AmountOfSetVowel[c];
						AmountOfSetVowel[c] = ++t;
					}
				}
			}
			return AmountOfSetVowel;
		}
	}
}