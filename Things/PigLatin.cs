﻿using System.Linq;
using AllTheTinyThings.FSM.Menus;

namespace AllTheTinyThings
{
	internal class PigLatin
	{
		//private char[] consos = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };

		public string PigLatinifier(string input)
		{
			// test case assuming Hello World
			var words = WordDetector(input);

			string output = string.Empty;

			foreach (string word in words)
			{
				if (word.Length <= 1)
				{
					output += word + " ";
					continue;
				}
				if (CommonActions.consos.Contains(word[0]))
				{
					string tmp = word;
					tmp = tmp.Substring(1, tmp.Length - 1);
					tmp += word.Substring(0, 1) + "ay";
					output += tmp + " ";
				}
				else
				{
					output += word + "way" + " ";
				}
			}
			return output;
		}

		private string[] WordDetector(string str)
		{
			return str.Split(' ');
		}
	}
}