﻿using System;
using System.Linq;

namespace AllTheTinyThings
{
	internal class Palindrome
	{
		public string IsPalindrome(string str)
		{
			char[] arr = str.Reverse().ToArray();

			string rev = new string(arr);
			if (str == rev)
			{
				return "Yup";
			}
			return "Nope";
		}
	}
}