﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings
{
	class Reverser
	{
		public string FlipString(string input)
		{
			string ret = string.Empty;

			for (int i = input.Length; i > 0; i--)
			{
				ret += input[i - 1];
			}
			return ret;
		}
	}
}
