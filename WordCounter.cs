﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings
{
	class WordCounter
	{
		public int CounterOfWords(string str)
		{
			return str.Split(' ').Count();
		}
	}
}
