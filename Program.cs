﻿using AllTheTinyThings.FSM;
using AllTheTinyThings.FSM.Menus;
using System;

namespace AllTheTinyThings
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			StateMachine.MenuInstance.AddState(new MainMenuState("main menu"));
			while (StateMachine.MenuInstance.Update())
			{
			}
		}
	}
}