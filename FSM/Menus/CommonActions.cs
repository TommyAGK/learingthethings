﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings.FSM.Menus
{
	public static class CommonActions
	{

		public static char[] consos = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };
		public static char[] vowels = {'a','e', 'i', 'o', 'u' }; // etc.

		public static bool RepeatOrReturn(ref State state)
		{
			bool run = true;
			Menu m = new Menu(new string[] { "Try Again.", "Back." });
			var k = m.GetMenuSelection();
			switch (k)
			{
				case 0:
					StateMachine.MenuInstance.RemoveState();
					StateMachine.MenuInstance.AddState(state);
					break;
				case 1:
					StateMachine.MenuInstance.RemoveState();
					break;


				default:
					break;
			}


			return run;
		}
	}
}
