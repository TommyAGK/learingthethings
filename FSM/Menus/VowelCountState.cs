﻿using System;

namespace AllTheTinyThings.FSM.Menus
{
	internal class VowelCountState : State
	{
		public VowelCountState(string name) : base(name)
		{
		}

		public override void Enter()
		{
			Console.Clear();
			Console.Write("Please enter a string to count: ");
			var v = new VowelCounter();
			string s = Console.ReadLine();
			Console.WriteLine("There are "+ v.GetVowelCount(s) +" vowels");
			var x = v.GetExactVowelCount(s);
			foreach (var i in x)
			{
				Console.WriteLine(i.Key + ": " +i.Value);
			}

		}

		public override void Exit()
		{
		}

		public override bool Run()
		{
			State state = this;
			return CommonActions.RepeatOrReturn(ref state);
		}
	}
}