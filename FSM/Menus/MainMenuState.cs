﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings.FSM.Menus
{
	class MainMenuState : State
	{
		public MainMenuState(string name) : base(name)
		{
		}

		public override void Enter()
		{
			Console.Clear();
		}

		public override void Exit()
		{
			// no current actions
		}

		public override bool Run()
		{
			bool run = true;

			Menu m = new Menu(new string[] { "Flip String.", "Pig Latin.", "Count Vovels","Check if Palindrome", "Word counter","Exit." });
			var k = m.GetMenuSelection();
			switch (k)
			{
				case 0:
					// go to flip string state
					StateMachine.MenuInstance.AddState(new FlipStringState("Flip string."));
					break;

				case 1:
					// go to pig latin state
					StateMachine.MenuInstance.AddState(new PigLatinState("Pig latin."));
					break;

				case 2:
					StateMachine.MenuInstance.AddState(new VowelCountState("Vowel counter."));
					break;

				case 3:
					StateMachine.MenuInstance.AddState(new PalindromeState("Palindrome check."));
					break;

				case 4:
					StateMachine.MenuInstance.AddState(new WordCountState("Word counter."));
					break;				

				default:
					run = false;
					break;
			}
			return run;
		}
	}
}
