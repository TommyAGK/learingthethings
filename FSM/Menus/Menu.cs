﻿using System;

namespace AllTheTinyThings.FSM.Menus
{
	public class Menu
	{
		private string[] _menuItems;
		private bool loopCompleted;
		private int topOffset = Console.CursorTop;
		private int bottomOffset = 0;
		private int selectedItem = 0;
		private ConsoleKeyInfo kb;

		public Menu(string[] menuItems)
		{
			_menuItems = menuItems;
		}

		private int DrawMenu(string[] iArr)
		{
			Console.CursorVisible = false;

			if ((iArr.Length) > Console.WindowHeight)
			{
				throw new Exception("Too many items in menu array");
			}

			// draw it
			while (!loopCompleted)
			{
				for (int i = 0; i < iArr.Length; i++)
				{
					if (i == selectedItem)
					{
						// Current menu line is selected, make it show that!
						Console.BackgroundColor = ConsoleColor.Black;
						Console.ForegroundColor = ConsoleColor.Cyan; // I like Cyan.
						Console.WriteLine(">" + iArr[i]);
						Console.ResetColor();
					}
					else
					{
						//print whatever is not selected.
						Console.WriteLine(" " + iArr[i]);
					}
				}
				HandleInput(ref iArr);
				bottomOffset = Console.CursorTop;

				Console.SetCursorPosition(0, topOffset);
			}
			Console.SetCursorPosition(0, bottomOffset);
			Console.CursorVisible = true;
			return selectedItem;
		}

		private void HandleInput(ref string[] iArr)
		{
			// input
			kb = Console.ReadKey(true);

			switch (kb.Key)
			{
				case ConsoleKey.UpArrow:
					if (selectedItem > 0)
					{
						selectedItem--;
					}
					else
					{
						selectedItem = (iArr.Length - 1);
					}
					break;

				case ConsoleKey.DownArrow:
					if (selectedItem < (iArr.Length - 1))
					{
						selectedItem++;
					}
					else
					{
						selectedItem = 0;
					}
					break;

				case ConsoleKey.Enter:
					loopCompleted = true;
					break;
			}
		}

		public int GetMenuSelection()
		{
			return DrawMenu(_menuItems);
		}

		public bool YesNo()
		{
			var yesNo = new Menu(new string[] { "Yes", "No" });
			if (yesNo.GetMenuSelection() == 0) // its a yes
			{
				return true;
			}
			return false;
		}
	}
}