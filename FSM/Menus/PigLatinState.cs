﻿using System;

namespace AllTheTinyThings.FSM.Menus
{
	internal class PigLatinState : State
	{
		

		public PigLatinState(string name) : base(name)
		{
		}

		public override void Enter()
		{
			Console.Clear();
			Console.Write("Please enter a string to piglatinify: ");
			var p = new PigLatin();
			Console.WriteLine(p.PigLatinifier(Console.ReadLine()));
		}

		public override void Exit()
		{
		
		}

		public override bool Run()
		{
			State state = this;
			return CommonActions.RepeatOrReturn(ref state);
		}
	}
}