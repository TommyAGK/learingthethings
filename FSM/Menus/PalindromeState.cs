﻿using System;

namespace AllTheTinyThings.FSM.Menus
{
	internal class PalindromeState : State
	{
		public PalindromeState(string name) : base(name)
		{
		}

		public override void Enter()
		{
			Console.Clear();
			Console.Write("Please enter a string to check for palindrome: ");
			var p = new Palindrome();
			Console.WriteLine(p.IsPalindrome(Console.ReadLine()));
		}

		public override void Exit()
		{
		}

		public override bool Run()
		{
			State state = this;
			return CommonActions.RepeatOrReturn(ref state);
		}
	}
}