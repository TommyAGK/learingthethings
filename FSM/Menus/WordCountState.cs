﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings.FSM.Menus
{
	class WordCountState : State
	{
		public WordCountState(string name) : base(name)
		{
		}

		public override void Enter()
		{
			Console.Clear();
			var c = new WordCounter();
			Console.WriteLine("There are : " + c.CounterOfWords(Console.ReadLine()) + " words.");
		}

		public override void Exit()
		{

		}

		public override bool Run()
		{
			State state = this;
			return CommonActions.RepeatOrReturn(ref state);
		}
	}
}
