﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings.FSM.Menus
{
	class FlipStringState : State
	{
		public FlipStringState(string name) : base(name)
		{
		}

		public override void Enter()
		{
			Console.Clear();
			Console.Write("Please enter a string to flip: ");
			var r = new Reverser();
			Console.WriteLine(r.FlipString(Console.ReadLine()));
		}

		public override void Exit()
		{
			Console.Clear();
			
		}

		public override bool Run()
		{
			State state = this;
			return CommonActions.RepeatOrReturn(ref state);
		}
	}
}
