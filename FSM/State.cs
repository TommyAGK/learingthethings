﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings.FSM
{
	public abstract class State
	{
		// basic state, for a statemachine

		public string Name { get; private set; }

		public State(string name)
		{
			Name = name;
		}

		public abstract void Enter();
		public abstract void Exit();
		public abstract bool Run();
	}
}
