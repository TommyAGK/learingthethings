﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllTheTinyThings.FSM
{
	public class StateMachine
	{
		// singleton...
		static StateMachine menuInstance;
		static StateMachine actionInstance;


		// collection

		Stack<State> states = new Stack<State>();

		public static StateMachine MenuInstance
		{
			get
			{
				if (menuInstance == null)
				{
					menuInstance = new StateMachine();
				}
				return menuInstance;
			}
		}

		public static StateMachine ActionInstance
		{
			get
			{
				if(actionInstance == null)
				{
					actionInstance = new StateMachine();
				}
				return actionInstance;
			}
		}

		public void AddState(State state)
		{
			state.Exit();
			states.Push(state);
			state.Enter();
		}

		public void RemoveState()
		{
			var x = states.Peek();
			x.Exit();

			states.Pop();
			var y = states.Peek();
			y.Enter();
		}
		
		public bool Update()
		{
			var u = states.Peek();
			return u.Run();
		}


	}
}
